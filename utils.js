"use strict";

// Some utils

function isArray(a) {
  return a instanceof Array;
}

function slice(col, from, to) {
  return Array.prototype.slice.call(col, from, to);
}

function rest(ary) {
  return slice(ary, 1);
}

function first(ary) {
  return ary[0];
}

function second(ary) {
  return ary[1];
}

function third(ary) {
  return ary[2];
}

function last(ary) {
  return ary[ary.length - 1];
}

function concat() {
  return Array.prototype.concat.apply([], arguments);
}

function append(list, item) {
  return concat(list, [item]);
}

function shift(ary) {
  return Array.prototype.shift.call(ary);
}

function pop(ary) {
  return Array.prototype.pop.call(ary);
}

function fapply(fn, args) {
  return fn.apply(null, args);
}

function mapply(fns, list) {
  return fns.reduce(function(result, fn, i) {
    return append(result, fn(list[i]));
  }, []);
}

function invoker(method) {
  return function(obj) { return obj[method].apply(obj, rest(arguments)); };
}

function splat(fn) {
  return function() { return fapply(fn, first(arguments)); };
}

function repeatedly(fn, times) {
  var result = [];
  for (var i=0; i<times; i++) { result.push(fn()); }
  return result;
}

function repeat(thing, times) {
  var result = [];
  for (var i=0; i<times; i++) { result.push(thing); }
  return result;
}

function ncurry(fn) {
  var rightArgs = rest(arguments);
  return function() { return fapply(fn, concat(slice(arguments), rightArgs)); };
}

function partial(fn) {
  var leftArgs = rest(arguments);
  return function() { return fapply(fn, concat(leftArgs, slice(arguments))); };
}

function unary(fn) {
  return function(a1) { return fn(a1); };
}

function binary(fn) {
  return function(a, b) { return fn(a, b); };
}

function pipe() {
  var fns = slice(arguments);
  return function() {
    return rest(fns).reduce(function(result, fn) {
      return fn(result);
    }, fapply(first(fns), arguments));
  };
}

function list(v) {
  return [v];
}

function asList(ary) {
  return isArray(ary)? ary : list(ary);
}

function k(v) {
  return function() { return v; };
}

function existy(v) {
  return (v !== null) && (v !== undefined);
}

function maybe(fn) {
  return function(arg) { return existy(arg)? fn(arg) : void 0; };
}

function callMaybe(fn) {
  return existy(fn)? fapply(fn, rest(arguments)) : void 0;
}

function callWhen(v, fn) {
  if (v) { return fn(v); }
}

function mapWith(fn) {
  return function(col) { return col.map(fn); };
}

function map(col, fn) {
  return col.map(unary(fn));
}

function result(v) {
  return isFunction(v)? fapply(v, rest(arguments)) : v;
}

function join(ary, separator) {
  return ary.join(separator);
}

function prepend(str, pre) {
  return join([pre, str], "");
}

function appendStr(str, post) {
  return join([str, post], "");
}

function tap(fn, value) {
  fn(value);
  return value;
}

function tapLog(value) {
  return tap(console.log.bind(console, "[TAPLOG:]"), value);
}

function tapDebug(msg) {
  return function(value) {
    return tap(console.log.bind(console, msg), value);
  };
}

function changeValue(orig, target, otherwise) {
  return function(v) { return v === orig? target : otherwise; };
}

function changer(orig, targetFn, otherwiseFn) {
  return function(v) { return v === orig? targetFn(v) : otherwiseFn(v); };
}

function inPairs(col) {
  var result = [];
  for (var i=0,_len=col.length; i<_len;i += 2) {
    result.push([col[i], col[i+1]]);
  }
  return result;
}

function identity(v) {
  return v;
}

function serialize(v) {
  return typeof(v) === "string"? v : JSON.stringify(v);
}

function copyList(l) {
  return Array.prototype.slice.call(l);
}

function reduce(l) {
  return Array.prototype.reduce.apply(l, slice(arguments, 1));
}

function merge() {
  return reduce(arguments, function(acc, el) {
    for (var k in el) if (el.hasOwnProperty(k)) { acc[k] = el[k]; }
    return acc;
  }, {});
}

function not(fn) {
  return function() { return !fapply(fn, arguments); };
}

function treeMap(nodep, children, combine, fn, tree) {
  if (nodep(tree))
    return combine(fn(tree), map(children(tree),
                                 partial(treeMap, nodep, children, combine, fn)));
  else
    return fn(tree); // leave
}

function dispatcher(cases, extractor) {
  if (!existy(extractor)) { extractor = identity; }
  return function(v) {
    for (var k in cases) if (extractor(v) === k)
      return cases[k](v);
    return callMaybe(cases._default, v);
  };
}

function dispatcherp() {
  var cases = arguments,
      defaultFn = isArray(last(cases))? identity : pop(cases);
  return function(v) {
    var pred, fn;
    for (var i=0, len=cases.length; i<len; i++) {
      pred = cases[i][0];
      fn = cases[i][1];
      console.log(v, pred);
      if (pred(v)) return fn(v);
    }
    return defaultFn(v);
  };
}

function or() {
  return reduce(arguments, function(a, b) { return a || b; });
}

function and() {
  return reduce(arguments, function(a, b) { return a && b; });
}

function checker(op) {
  var predicates = slice(arguments, 1);
  return function(v) {
    return fapply(op, mapply(predicates, repeat(v, predicates.length)));
  };
}
