"use strict";

var macros = {
  transformations: {
  },

  createMacro: function(astNode, macroStore) {
    var name = null,
        fn = macros.compileMacro(astNode);
    macroStore[name] = fn;
  },

  compileMacro: function (astNode) {
  },

  getMacro: function (name, macroStore) {
    return macroStore[name];
  },

  transformWithMacro: function (name, ast, macroStore) {
    return macros.getMacro(name, macroStore)(ast);
  },

  visitNode: function(node) {
  },

  visitTree: function(ast) {
  },

  expandSexp: function(astNode) {

  },

  expandTree: function(astList) {
    return map(astList, macros.expandSexp);
  },

  // quasiquote is A COMPILATION TIME "MACRO"
  // ...so, substitute every aparition of ` with the result of
  // ...the call (kind of an "interpreter" of the macro "dsl")
  quasiquote: function(children) {
    // 1. Quoted: return just the tree node
    // 2. Unquoted: return the compiled expression
    // 3.
  },

  quote: function(node) {
    return node;
  },

  unquote: function(node) {
    return compile(node); // A STRING!!
  },

  unquoteSplice: function(children) {
    // just map unquote and return the LIST OF STRINGS
  },

};

function expandMacros(astList) {
  var createdMacros = {};
  map(astList, pipe(
    macros.isMacro,
    maybe(ncurry(macros.createMacro, createdMacros))
  ));
  return macros.visitTree(ast, macros);
}

// 1. Visit the three, depth first
// 2. Register any macro definitions remove them form the AST
// 3. Expand the detected macro calls (recursive to 1)
// 4. Return the expanded tree to the next stage
// 5. MACROS CAN ONLY BE TOP-LEVEL DEFINITIONS
