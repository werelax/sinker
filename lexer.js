"use strict";

/* (1) Lexer */

function lexer(code) {
  var strings = [],
      normalize = function(code) {
        return code.replace(/\(/g, " ( ")
                   .replace(/\)/g, " ) ")
                   .replace(/\s\s*/g, " ")
                   .replace(/^\s*/g, "")
                   .replace(/\s*$/g, "");
      };
  code = code.replace(/;.*\n/g, "\n")
             .replace(/(".*?")/g, function(str) { return "ç" + (strings.push(str) - 1) + "ç"; });
  code = normalize(code)
         .replace(/([a-zA-Zñ_\-eval\+\?\*><\/\%!=#]\S*)/g, "\"$1\"")
         .replace(/ç(\d+)ç/g, "strings[$1]")
         .replace(/\(\s*/g, "[")
         .replace(/\s*\)/g, "]")
         .split(" ").join(", ");
  code = ["[", code, "]"].join("");
  console.log(code);
  return eval(code);
}

/* (2) Parser */
function isFunction(f) {
  return typeof(f) === "function";
}

function createPredicate(match) {
  var regex = new RegExp("^"+match+"$");
  return function(e) { return e.match && e.match(regex); };
}

function createCompPredicate(match) {
  var p = createPredicate(match);
  return function(e) { return isArray(e) && p(e[0]); };
}

var typePredicates = {
  "number": function(n) { return !isNaN(parseFloat(n)); },
  "string": createPredicate("\".*\""),
  "boolean": createPredicate("#[f|t]"),
  "lambda": createCompPredicate("lambda"),
  "lambda2": createCompPredicate("l"),

  "eq-exp": createCompPredicate("=="),
  "neq-exp": createCompPredicate("!="),
  "lt-exp": createCompPredicate("<"),
  "gt-exp": createCompPredicate(">"),
  "lte-exp": createCompPredicate("<="),
  "gte-exp": createCompPredicate(">="),
  "add-exp": createCompPredicate("\\+"),
  "mult-exp": createCompPredicate("\\*"),
  "div-exp": createCompPredicate("\\/"),
  "sub-exp": createCompPredicate("\\-"),
  "mod-exp": createCompPredicate("\\%"),
  "and-exp": createCompPredicate("and"),
  "or-exp": createCompPredicate("or"),
  "xor-exp": createCompPredicate("xor"),
  "not-exp": createCompPredicate("not"),
  "in-exp": createCompPredicate("in"),

  // "zero?": createCompPredicate("zero\\?"),

  "set-exp": createCompPredicate("set!"),
  "deref-exp": createCompPredicate("get"),
  "set-ref-exp": createCompPredicate("set-ref"),

  "new-exp": createCompPredicate("new"),

  "map": createCompPredicate("obj"),
  "array": createCompPredicate("array"),

  "if-exp": createCompPredicate("if"),
  "for-statement": createCompPredicate("for"),
  "for-in-statement": createCompPredicate("for-in"),
  "while-statement": createCompPredicate("while"),
  "try-statement": createCompPredicate("try-catch"),
  "switch-statement": createCompPredicate("switch"),
  "break-statement": createCompPredicate("break"),
  "continue-statement": createCompPredicate("continue"),
  "debugger-statement": createCompPredicate("debugger"),
  "delete-statement": createCompPredicate("delete"),
  "instanceof-exp": createCompPredicate("instance-of"),
  "throw-exp": createCompPredicate("throw!"),
  "typeof-exp": createCompPredicate("type-of"),
  "void-exp": createCompPredicate("void"),

  "begin-exp": createCompPredicate("begin"),

  // "let": createCompPredicate("let"),
  // "letrec": createCompPredicate("letrec"),

  "setref": createCompPredicate("set!"),
  "defun": createCompPredicate("defun"),
  "defun2": createCompPredicate("function"),

  "js-return": createCompPredicate("ret"),
  "js-var": createCompPredicate("var"),

  // macro stuff

  "defmacro": createCompPredicate("defmacro"),
  "quote": createCompPredicate("quote"),
  "quasiquote": createCompPredicate("quasiquote"),
  "unquote": createCompPredicate("unquote"),
  "unquote-splice": createCompPredicate("unquote-splice")
};

function buildNode(type, value) {
  return {
    type: type,
    value: (isArray(value)? undefined : value),
    childs: (isArray(value)? parseTree(value.slice(1)) : undefined)
  };
}

function parseExp(token) {
  for (var type in typePredicates) {
    if (typePredicates[type](token)) {
      return buildNode(type, token);
    }
  }
  return (isArray(token)? parseTree(token) : buildNode("symbol", token));
}

function parseTree(tokens) {
  return tokens.map(parseExp);
}
