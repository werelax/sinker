"use strict";

function transform(code) {
  var ast = parseTree(lexer(code));
  console.log(ast);
  var tst = mapWith(transformNode)(ast);
  console.log("tst:", tst);
  var targetCode = mapWith(compile)(tst);
  console.log("target:", targetCode);
  // var tst = mapWith(transformNode)(ast),
  //     targetCode = mapWith(compile)(tst);
  // console.log(tst);
  // console.log(targetCode);
  // return targetCode.concat("").join(";\n\n");
  return targetCode.concat("").join(";\n\n");
}

// Missin words:
//
// X do
// X with
// _ in
// _ try
// _ catch
// _ finally
// _ switch
// _ case
// _ default
// _ break
// _ continue
// _ debugger
// _ delete
// _ else
// _ for
// _ function
// _ if
// _ instanceof
// _ new
// _ return
// _ this
// _ throw
// _ typeof
// _ var
// _ void
// _ while

// Helpers
//
// _(begin)[done]_
// aplicative funcs/macros: (for-each) _(for-in)[done]_ (do [lisp syntax])
// (when)

// Next step:
//
// 1. List manipulation functions / prelude of operations
// 2. 3 pass transformation: macro compilation/macro expansion/conversion
// 3. TargetS.Tree analysis for code enhancements (implicit return, semicolon insertion, if-as-expression)
