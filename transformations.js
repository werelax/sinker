"use strict";

/* (2) (Abstract S. Tree -> Target S. Tree) transformations */

// AST Node operations

function cnth(i) {
  return function(node) { return node.childs[i]; };
}

function crest(i) {
  return function(node) { return node.childs.slice(i); };
}

function accesor(field) {
  return function(node) { return node[field]; };
}

function setter(field) {
  return function(obj, value) { obj[field] = value; return obj; };
}

function buildTstNode(type, value, expressions, blocks) {
  return {
    type: type,
    value: value,
    expressions: expressions,
    blocks: blocks
  };
}

function tstSpecialForm(type, extractors) {
  return function(astNode) {
    return buildTstNode(
      type,
      result(extractors.value, astNode),
      maybe(mapWith(partial(callMaybe, extractors.expressionNodeTransformation)))(callMaybe(extractors.expressions, astNode)),
      maybe(mapWith(maybe(mapWith(partial(callMaybe, extractors.blockNodeTransformation)))))(callMaybe(extractors.blocks, astNode))
    );
  };
}

function tstTransformation(type, extractors) {
  extractors = merge(extractors, {
    expressionNodeTransformation: transformNode,
    blockNodeTransformation: transformNode
  });
  return tstSpecialForm(type, extractors);
}

var tstTransforms = {
  "number": tstTransformation("value", {value: accesor("value")}),
  "string": tstTransformation("value", {value: accesor("value")}),
  "symbol": tstTransformation("symbol", {value: accesor("value")}),
  "boolean": tstTransformation("symbol", {value: pipe(accesor("value"), changeValue("#t", "true", "false")) }),
  "array": tstTransformation("array", {value: "", expressions: crest(0)}),
  "map": tstTransformation("map", {value: "", expressions: crest(0)}),

  "lambda": tstTransformation("exp-fun", {value: "", expressions: cnth(0), blocks: pipe(crest(1), list)}),
  "lambda2": tstTransformation("exp-fun", {value: "", expressions: cnth(0), blocks: pipe(crest(1), list)}),
  "defun": tstTransformation("def-fun", {value: cnth(0), expressions: cnth(1), blocks: pipe(crest(2), list)}),
  "defun2": tstTransformation("def-fun", {value: cnth(0), expressions: cnth(1), blocks: pipe(crest(2), list)}),

  "add-exp": tstTransformation("exp-infix", {value: " + ", expressions: accesor("childs") }),
  "sub-exp": tstTransformation("exp-infix", {value: " - ", expressions: accesor("childs") }),
  "mult-exp": tstTransformation("exp-infix", {value: " * ", expressions: accesor("childs") }),
  "div-exp": tstTransformation("exp-infix", {value: " / ", expressions: accesor("childs") }),
  "mod-exp": tstTransformation("exp-infix", {value: " % ", expressions: accesor("childs") }),
  "eq-exp": tstTransformation("exp-infix", {value: " === ", expressions: accesor("childs") }),
  "neq-exp": tstTransformation("exp-infix", {value: " !== ", expressions: accesor("childs") }),
  "lt-exp": tstTransformation("exp-infix", {value: " < ", expressions: accesor("childs") }),
  "gt-exp": tstTransformation("exp-infix", {value: " > ", expressions: accesor("childs") }),
  "lte-exp": tstTransformation("exp-infix", {value: " <= ", expressions: accesor("childs") }),
  "gte-exp": tstTransformation("exp-infix", {value: " >= ", expressions: accesor("childs") }),
  "and-exp": tstTransformation("exp-infix", {value: " && ", expressions: accesor("childs") }),
  "or-exp": tstTransformation("exp-infix", {value: " || ", expressions: accesor("childs") }),
  "xor-exp": tstTransformation("exp-infix", {value: " ^ ", expressions: accesor("childs") }),
  "not-exp": tstTransformation("exp-prefix", {value: "!", expressions: accesor("childs") }),
  "in-exp": tstTransformation("exp-infix", {value: "in", expressions: pipe(crest(0), ncurry(slice, 0, 2)) }),

  "set-exp": tstTransformation("set-exp", {value: pipe(cnth(0), transformNode), expressions: crest(1) }),
  "deref-exp": tstTransformation("deref", {value: pipe(cnth(0), transformNode), expressions: crest(1)}),

  "if-exp": tstTransformation("if-exp", {expressions: pipe(cnth(0), list), blocks: pipe(crest(1), mapWith(list))}),
  "while-statement": tstTransformation("while-stmnt", {expressions: pipe(cnth(0), list), blocks: pipe(crest(1), list)}),
  "for-statement": tstTransformation("for-stmnt", {expressions: pipe(crest(0), ncurry(slice, 0, 3)), blocks: pipe(crest(3), list)}),
  "for-in-statement": tstTransformation("for-in-stmnt", {expressions: cnth(0), blocks: pipe(crest(1), list)}),
  "try-statement": tstTransformation("try-stmnt", {blocks: pipe(crest(0), mapWith(list))}),
  "switch-statement": tstTransformation("switch-stmnt", {expressions: pipe(cnth(0), list), blocks: crest(1)}),

  "begin-exp": tstTransformation("begin-exp", {blocks: pipe(crest(0), list)}),

  "funcall": tstTransformation("funcall", {value: pipe(first, transformNode), expressions: rest}),
  "js-return": tstTransformation("reserved-word", {value: "return", expressions: pipe(cnth(0), list)}),
  "js-var": tstTransformation("var-declaration", {value: "var", expressions: crest(0)}),
  "break-statement": tstTransformation("reserved-word", {value: "break"}),
  "continue-statement": tstTransformation("reserved-word", {value: "continue"}),
  "debugger-statement": tstTransformation("reserved-word", {value: "debugger"}),
  "delete-statement": tstTransformation("reserved-word", {value: "delete", expressions: pipe(cnth(0), list)}),
  "instanceof-exp": tstTransformation("exp-infix", {value: " instanceof ", expressions: pipe(crest(0), ncurry(slice, 0, 2))}),
  "throw-exp": tstTransformation("reserved-word", {value: "throw", expressions: pipe(cnth(0), list)}),
  "typeof-exp": tstTransformation("reserved-word", {value: "typeof", expressions: pipe(cnth(0), list)}),
  "void-exp": tstTransformation("reserved-word", {value: "void", expressions: pipe(cnth(0), list)}),

  "new-exp": tstTransformation("reserved-word", {value: "new", expressions: pipe(crest(0), list)}),

  // metaprogramming

  "quote": tstSpecialForm("meta-quote", {expressions: crest(0), expressionNodeTransformation: identity}),
  "quasiquote": tstSpecialForm("meta-quasiquote",
                               { expressions: crest(0),
                                 expressionNodeTransformation: partial(treeMap,
                                                                       dispatcher({ unquote: k(false),
                                                                                    "unquote-splice": k(false),
                                                                                    _default: checker(or,
                                                                                                      pipe(accesor("childs"),
                                                                                                           existy),
                                                                                                      isArray) },
                                                                                  accesor("type")),
                                                                       dispatcherp([isArray, identity], accesor("childs")),
                                                                       setter("childs"),
                                                                       dispatcher({ unquote: transformNode,
                                                                                    "unquote-splice": transformNode,
                                                                                    _default: identity },
                                                                                  accesor("type"))) }),
  "unquote": tstTransformation("meta-unquote", {expressions: crest(0)}),
  "unquote-splice": tstTransformation("meta-unquote-splice", {expressions: crest(0)})
};

function transformNode(astNode) {
  if (isArray(astNode))
    return callMaybe(tstTransforms.funcall, astNode);
  else
    return callMaybe(tstTransforms[astNode.type], astNode);
}
