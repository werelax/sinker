"use strict";

/* (3) Compilation / translation */

var getExp = accesor("expressions");
var getBlock = pipe(accesor("blocks"), first);
var getValue = accesor("value");

function transExp(exp, omitParens) {
  return [omitParens?"":"(", exp.map(compile).join(", "), omitParens?"":")"].join("");
}

function transBlock(block) {
  return ["{", block.map(compile).join(";\n") + ";", "}"].join("\n");
}

function tstTranslation(parts) {
  return function(node) {
    return parts.map(unary(ncurry(fapply, list(node))))
                .join("");
  };
}

var tstTranslations = {
  "symbol": tstTranslation([getValue]),
  "value": tstTranslation([getValue]),
  "array": tstTranslation([k("["),
                          pipe(getExp, ncurry(transExp, true)),
                          k("]")]),

  "map": tstTranslation([k("{"),
                        pipe(getExp,
                             inPairs,
                             mapWith(pipe(partial(mapply, [getValue, compile]),
                                          ncurry(join, ":")))),
                         k("}")]),

  "exp-infix": tstTranslation([k("("),
                                 pipe(ncurry(repeat ,2),
                                      partial(mapply, [pipe(getExp,
                                                            mapWith(compile)),
                                                            getValue]),
                                      splat(join)),
                               k(")")]),

  "exp-prefix": tstTranslation([getValue,
                               pipe(getExp, first, compile)]),

  "if-exp": tstTranslation([k("if"),
                           pipe(getExp, transExp),
                           pipe(accesor("blocks"),
                                first,
                                transBlock),
                           pipe(accesor("blocks"),
                                second,
                                maybe(transBlock),
                                maybe(ncurry(prepend, " else ")))]),

  "while-stmnt": tstTranslation([k("while"),
                                pipe(getExp, transExp),
                                pipe(getBlock, transBlock)]),

  "for-stmnt": tstTranslation([k("for ("),
                              pipe(getExp, first, maybe(compile)),
                              k(";"),
                              pipe(getExp, second, maybe(compile)),
                              k(";"),
                              pipe(getExp, third, maybe(compile)),
                              k(")"),
                              pipe(getBlock, transBlock)]),

  "for-in-stmnt": tstTranslation([k("for (var "),
                                  pipe(getExp, first, compile),
                                  k(" in "),
                                  pipe(getExp, second, compile),
                                  k(")"),
                                  pipe(getBlock, transBlock)]),

  "try-stmnt": tstTranslation([k("try "),
                              pipe(accesor("blocks"),
                                   first,
                                   transBlock),
                              pipe(accesor("blocks"),
                                   second,
                                   maybe(transBlock),
                                   maybe(ncurry(prepend, " catch(e) "))),
                              pipe(accesor("blocks"),
                                   third,
                                   maybe(transBlock),
                                   maybe(ncurry(prepend, " finally ")))]),


  "switch-stmnt": tstTranslation([k("switch ("), pipe(getExp, first, compile), k(") {\n"),
                                 pipe(accesor("blocks"),
                                      mapWith(pipe(
                                        partial(tap, identity),
                                        ncurry(repeat, 2),
                                        partial(mapply, [
                                          pipe(first,
                                               compile,
                                               changer("default", k("default:"), pipe(ncurry(prepend, "case "),
                                                                                      ncurry(appendStr, ":")))),
                                          pipe(rest,
                                               mapWith(compile),
                                               ncurry(append, "break;"),
                                               ncurry(join, ";\n"))]),
                                          ncurry(join, ""))),
                                     ncurry(join, "")),
                                 k("}")]),

  "begin-exp": tstTranslation([pipe(accesor("blocks"), first, mapWith(compile), tapLog, ncurry(join, ";\n"))]),

  "funcall": tstTranslation([pipe(getValue, compile),
                            pipe(getExp, transExp)]),

  "exp-fun": tstTranslation([k("(function "),
                             pipe(getExp, transExp),
                             pipe(getBlock, transBlock),
                             k(")")]),

  "def-fun": tstTranslation([k("function "),
                            pipe(getValue,
                                 partial(callMaybe, compile)),
                            pipe(getExp, transExp),
                            pipe(getBlock, transBlock)]),

  "var-declaration": tstTranslation([k("var "),
                                    pipe(getExp,
                                         inPairs,
                                         mapWith(pipe(partial(mapply, [getValue,
                                                                       maybe(compile)]),
                                                      ncurry(join, " = "))),
                                         ncurry(join, ", "))]),

  "reserved-word": tstTranslation([getValue,
                                  k(" "),
                                  pipe(getExp,
                                       maybe(mapWith(compile)))]),

  "set-exp": tstTranslation([pipe(getValue, compile),
                            k(" = "),
                            pipe(getExp, first, compile)]),

  "deref": tstTranslation([pipe(getValue, compile),
                          k("["),
                          pipe(getExp, first, compile),
                           k("]")]),

  // metaprogramming

  "meta-quote": tstTranslation([pipe(getExp, serialize)]),
  "meta-quasiquote": tstTranslation([k("["),
                                     pipe(getExp,
                                          tapLog,
                                          mapWith(pipe(partial(treeMap,
                                                               pipe(accesor("childs"), existy),
                                                               accesor("childs"),
                                                               function(node, children) {
                                                                 var chil =
                                                                     join(["[", mapWith(serialize)(children).join(", "), "]"], "");
                                                                 return isArray(node) ?
                                                                   chil
                                                                   : ["{", "type: \"", node.type, "\", ",
                                                                      "value: \"", node.value, "\", ",
                                                                      "childs: ", chil, "}"].join("");
                                                               },
                                                               dispatcher({ "meta-unquote": compile,
                                                                            "meta-unquote-splice": compile,
                                                                            _default: identity },
                                                                          accesor("type"))),
                                                       serialize)),
                                          ncurry(join, ", ")
                                         ),
                                     k("]")]),
  "meta-unquote": tstTranslation([pipe(getExp, mapWith(compile))]),
  "meta-unquote-splice": tstTranslation([pipe(getExp, mapWith(compile), tapDebug("WAT"))])
};

function compile(tstNode) {
  return callMaybe(tstTranslations[tstNode.type], tstNode);
}
